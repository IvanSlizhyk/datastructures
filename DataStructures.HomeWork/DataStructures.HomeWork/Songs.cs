﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataStructures.Homework
{
    /*
     * TASK: we should implement Songs storage, which maps Songs in the following way Name -> Artist (IDictionary<string, string>)
     * Choose data structure and implement existing methods. We don't need any additional methods by requirement
     * Try to make all methods effective. If you can't choose the exact structure - implement any and explain profs and cons
     */

    public class Songs
    {
        private Dictionary<string, string> _songStorage;

        public Songs()
        {
            _songStorage = new Dictionary<string, string>();
            /*
             * Songs internal storage initialization from external source here. 
             * Don't implement, suppose it's already loaded
             */
        }

        public bool ContainsSong(string songName)
        {

            // TODO: implement Song search by it's name
            return _songStorage.ContainsKey(songName);
        }

        public string GetSongArtist(string songName)
        {
            // TODO: implement Song Artist extraction by Song's name. null means - no Song found
            string value;
            _songStorage.TryGetValue(songName, out value);
            return value;
        }

        public void AddSong(string songName, string songArtist)
        {
            // TODO: Implement adding new Song to internal storage structure
            if (!_songStorage.ContainsKey(songName))
            {
                _songStorage.Add(songName, songArtist);
            }
        }

        public void RemoveSong(string songName)
        {
            // TODO: Implement removing existing Song from internal storage structure
            _songStorage.Remove(songName);
        }

        public string[] ExportSorted()
        {
            // TODO: implement export as sortedarray of songs' names. 
            string [] ss = _songStorage.Keys.ToArray();
            Array.Sort(ss);
            return ss;
        }
    }
}
