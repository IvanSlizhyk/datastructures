﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataStructures.Homework
{
    /*
     TASK: we are implementing class to check if number is odd or even
     * Suppose, that we want to store all odd number in the memory (it's strange, but let it be)
     * Replace internal data structure (field 'data') with more appropiate one and re-implement IsOdd method
     * Explain your data structure choice in data structure comment
     */


    /// <summary>
    /// Supports check if int non-negative number is odd
    /// </summary>
    public class OddChecker
    {
        /// <summary>
        /// Explain your choice HERE:
        /// </summary>
        ///int[] data1;
        private List<int> data;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="N">Max allowed number to check</param>
        public OddChecker(int N)
        {
            data = new List<int>();
            if (N < 0) return;
            int length = (N + 1) / 2;
            // Store all odd numbers less than N into internal array
            for (int i = 0; i < length; i++)
                data.Add(2 * i + 1);
        }

        /// <summary>
        /// Checks if number is odd
        /// </summary>
        /// <param name="number">Number to check (should be non-negative and not greater than N)</param>
        /// <returns>True if given number is odd</returns>
        public bool IsOdd(int number)
        {
            if (number < 0)
                return false;
            if (data.Contains(number))
                return true;
            return false;
        }
    }
}
