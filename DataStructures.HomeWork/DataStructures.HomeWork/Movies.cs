﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataStructures.Homework
{
    /*
     * TASK: we should implement Movies storage, which maps Movies in the following way Name -> Director (IDictionary<string, string>)
     * Choose data structure and implement existing methods. We don't need any additional methods by requirement
     * Try to make all methods effective. If you can't choose the exact structure - implement any and explain profs and cons
     */

    public class Movies
    {
        private Dictionary<string, string> _movieStorage;

        public Movies()
        {
            _movieStorage = new Dictionary<string, string>();
            /*
             * Movies internal storage initialization from external source here. 
             * Don't implement, suppose it's already loaded
             */
        }

        public bool ContainsMovie(string movieName)
        {

            // TODO: implement Movie search by it's name
            return _movieStorage.ContainsKey(movieName);
        }

        public string GetMovieDirector(string movieName)
        {
            // TODO: implement Movie Director extraction by Movie's name. null means - no Movie found
            string value;
            _movieStorage.TryGetValue(movieName, out value);
            return value;
        }

        public void AddMovie(string movieName, string movieDirector)
        {
            // TODO: Implement adding new movie to internal storage structure
            if (!_movieStorage.ContainsKey(movieName))
            {
                _movieStorage.Add(movieName, movieDirector);
            } 
        }

        public void RemoveMovie(string movieName)
        {
            // TODO: Implement removing existing movie from internal storage structure
            _movieStorage.Remove(movieName);
        }
    }
}
