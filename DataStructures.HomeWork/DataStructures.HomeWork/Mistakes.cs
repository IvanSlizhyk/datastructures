﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataStructures.Homework
{
    /*
     * TASK: correct all mistakes (5) in method CorrectMistakes
     * By the way finish MyKey implementation (mistake 3 asks to do it)
     */

    public static class Mistakes
    {
        public static void CorrectMistakes()
        {
            int N = 1000000;
            int[] data = new int[N];
            Random random = new Random();
            for (int i = 0; i < N; i++)
                data[i] = random.Next();

            // 1. Just correct a mistake
            var dictionary = new Dictionary<int, object>();
            foreach (var item in data)
                if (!dictionary.ContainsKey(item))
                {
                    dictionary.Add(item, null);
                }

            // 2. We want to manage input data as a queue. Correct a mistake
            List<int> queue = new List<int>(data);
            /*while (queue.Count > 0)
            {
                int item = queue[0];

                queue.RemoveAt(0);
                Console.WriteLine("Processing item: {0}", item);
            }*/

            // 3. Look at MyKey class: implement GetHashCode(), 
            // what is missing else to use MyKey as Key in Dictionary<MyKey, object> in the following code:
            var myKeyDicitonary = new Dictionary<MyKey, object>();
            myKeyDicitonary.Add(new MyKey("simple", 1), null);
            if (myKeyDicitonary.ContainsKey(new MyKey("simple", 1)))
                Console.WriteLine("MyKey is implemented correctly");
            else
                Console.WriteLine("MyKey is implemented in a wrong way (something is still missing)");

            // 4. Use SortedList more efficiently (we use dictionary from 1.)
            var sortedList = new SortedList<int, object>(dictionary);

            // 5. Try to find more efficient way to get dictionary entry 
            // - this implementation uses two dictionary methods and two hashcode calculations (dictionary from 1.)
            //if (dictionary.ContainsKey(100)/* <- first call */)
            //    Console.WriteLine("Dictionary entry exists: {0}->{1}", 100, dictionary[100]/* <- second call */);
            object s;
            if (dictionary.TryGetValue(100, out s))
            {
                Console.WriteLine("Dictionary entry exists: {0}->{1}", 100, s);
            }
        }

        class MyKey
        {
            public string KeyType { get; private set; }
            public int ID { get; private set; }

            public MyKey(string keyType, int id)
            {
                KeyType = keyType;
                ID = id;
            }

            public override int GetHashCode()
            {
                return this.ID.GetHashCode() + this.KeyType.GetHashCode();
            }

            public override bool Equals(object obj)
            {
                MyKey obj1 = obj as MyKey;
                if (obj1 == null)
                    return false;
                else
                    return this.ID == obj1.ID && this.KeyType == obj1.KeyType;
            }
        }
    }
}
