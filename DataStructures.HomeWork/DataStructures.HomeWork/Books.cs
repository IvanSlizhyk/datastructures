﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataStructures.Homework
{
    /*
     * TASK: we should implement books storage, which maps books in the following way Name -> Author (IDictionary<string, string>)
     * Choose data structure and implement existing methods. We don't need any additional methods by requirement
     * Try to make all methods effective. If you can't choose the exact structure - implement any and explain profs and cons
     */

    public class Books
    {
        private Dictionary<string, string> _bookStorage;

        public Books()
        {
            /*
             * Books internal storage initialization from external source here. 
             * Don't implement, suppose it's already loaded
             */           
            _bookStorage = new Dictionary<string, string>();
        }

        public bool ContainsBook(string bookName)
        {
            // TODO: implement book search by it's name
            return _bookStorage.ContainsKey(bookName);
        }

        public string GetBookAuthor(string bookName)
        {
            // TODO: implement book author extraction by book's name. null means - no book found
            string value;
            _bookStorage.TryGetValue(bookName, out value);
            return  value;
        }

        public void AddBook(string bookName, string bookAuthor)
        {
            if (!_bookStorage.ContainsKey(bookName))
            {
                _bookStorage.Add(bookName, bookAuthor);
            }
        }
    }
}
