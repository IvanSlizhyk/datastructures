﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataStructures.Homework;

namespace DataStructures.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            //Books
            var books = new Books();
            books.AddBook("Inferno","Dan Braun");
            books.AddBook("Angel and Demon", "Dan Braun");

            var isBookContain = books.ContainsBook("Inferno");
            System.Console.WriteLine(isBookContain ? "Done" : "Fail");

            var author = books.GetBookAuthor("Inferno");
            System.Console.WriteLine(author);

            //Mistakes
            Mistakes.CorrectMistakes();

            //Movies
            var movies = new Movies();
            movies.AddMovie("Inception", "Christopher Nolan");
            movies.AddMovie("The Prestige", "Christopher Nolan");

            var isMovieContain = movies.ContainsMovie("Inception");
            System.Console.WriteLine(isMovieContain ? "Done" : "Fail");

            var director = movies.GetMovieDirector("Inception");
            System.Console.WriteLine(director);

            movies.RemoveMovie("The Prestige");
            isMovieContain = movies.ContainsMovie("The Prestige");
            System.Console.WriteLine(isMovieContain ? "Done" : "Fail");

            //OddChecker
            var oddChecker = new OddChecker(10);
            System.Console.WriteLine(oddChecker.IsOdd(11) ? "Done" : "Fail");
            System.Console.WriteLine(oddChecker.IsOdd(7) ? "Done" : "Fail");
            System.Console.WriteLine(oddChecker.IsOdd(-7) ? "Done" : "Fail"); 

            //Songs
            var songs = new Songs();
            songs.AddSong("Hello", "Adele");
            songs.AddSong("Run Run Run", "Tokio Hotel");

            var isSongContain = songs.ContainsSong("Hello");
            System.Console.WriteLine(isSongContain ? "Done" : "Fail");

            var artist = songs.GetSongArtist("Hello");
            System.Console.WriteLine(artist);

            songs.RemoveSong("Run Run Run");
            isSongContain = songs.ContainsSong("Run Run Run");
            System.Console.WriteLine(isSongContain ? "Done" : "Fail");

            songs.AddSong("dfjgjgf", "dhhd");songs.AddSong("sdgsdg", "ghkhgk");
            songs.AddSong("hjhjlhjlhj", "dfhdf"); songs.AddSong("wetetwt", "sdtdd");
            var songsName = songs.ExportSorted();

            System.Console.ReadKey();
        }
    }
}
